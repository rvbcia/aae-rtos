#include <stdio.h>

#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>

RT_TASK demo_task;

void demo(void *arg) {
RT_TASK *curtask;
RT_TASK_INFO curtaskinfo;

// hello world
printf("Hello World!\n");

//inquire current task
curtask=rt_task_self();

rt_task_inquire(curtask,&curtaskinfo);

//print task name
printf("Task name : %s \n", curtaskinfo.name);
}

int main(int argc, char* argv []){

char str[10];

//Lock memory : avoid memory swapping for this program
mlockall(MCL_CURRENT|MCL_FUTURE);

printf("start task\n");

// 1. Create a task using a demo function:

sprintf(str, "hi");
rt_task_create(&demo_task, str, 0,50,0);

//2. Start a task:

rt_task_start(&demo_task, &demo, 0);

}
