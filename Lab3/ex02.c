#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <alchemy/task.h>
#include <alchemy/timer.h>

RT_TASK demo_task;
RT_TIMER_INFO timer;

void demo(void *arg) {
RT_TASK *curtask;
RT_TASK_INFO curtaskinfo;
RTIME tstart;

curtask = rt_task_self();
rt_task_inquire(curtask, &curtaskinfo);
int counter = 0;

printf("starting task %s\n", curtaskinfo.name);

// 1s task period
rt_task_set_periodic(NULL, TM_NOW, rt_timer_ns2ticks(1000*1000*1000));

tstart = rt_timer_read();

//start tas loop
while(1) {
	printf("Loop count: %d, Loop time: %.5f ms\n", counter, (rt_timer_read() - tstart)/1000000.0);
	counter++;
	rt_task_wait_period(NULL);
}
}

int main (int argc, char* argv[]){
char str[10];

mlockall(MCL_CURRENT|MCL_FUTURE);

sprintf(str,"hi");
rt_task_create(&demo_task, str, 0,50,0);
rt_task_start(&demo_task, &demo, 0);

pause();
}
