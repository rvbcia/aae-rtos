#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <sys/mman.h>
#include <alchemy/task.h>
#include <alchemy/queue.h>

#define THREADS 15

type def struct {
  unsigned iterations ;
  RT_QUEUE *queue ;
} arg_t;

void monte (void *arg) {
arg_t * a = ( arg_t *) arg ;
unsigned counter = 0;
  
  // calculation
  for ( size_ti =  i<a>iterations; i++) {
  float x = rand ( ) / ( float )RAND_MAX;
  float y = rand ( ) / ( float )RAND_MAX;
  float point =sqrt ( ( x*x ) + ( y* y ) ) ;
  if( point < 1 ) { counter++; }
}

// sending queue process
void *buf ;
int i = 0 ;

for ( ; ( ( buf = rt_ queue_ alloc ( a >queue , sizeof ( counter ) ) ) == NULL) && ( i < 100 ) ; ++i ) {
  rt_task_sleep (rand () % 10) ; } // it sends when buf is valid
if ( buf ) { *( unsigned *) buf = counter ;
rt_queue_send ( a >queue , buf , size of ( counter ) , Q_NORMAL) ;
} else { printf ( ) ; }
}

int main ( int argc , char *argv [ ] ) {
RT_TASK threads [THREADS] ;
RT_QUEUE queue ;
srand ( ( unsigned int ) time (NULL) ) ;
unsigned iterations = rand ( ) % 10000 + 100 ;
printf ( "%diterations per thread \n" , iterations ) ;
arg_t arg ;
arg.iterations = iterations ;
arg.queue = &queue ;
mlockall (MCL_CURRENT|MCL_FUTURE) ;
rt_queue_create(&queue , NULL, 64 , Q_UNLIMITED, Q_FIFO) ;
printf ( "starttasks \n" ) ;
for ( size_t i = 0 ; i < THREADS; ++i ) {
rt_task_create(& threads [ i ] , NULL, 0 , 10 , 0 ) ;
rt_task_start(& threads [ i ] , monte , &arg ) ;
}
unsigned sum = 0 ;
size_t done = 0 ;
unsigned recv ;
while ( done < THREADS) {
rt_task_sleep ( rand () % 100 + 10 ) ;
rt_queue_read(&queue , (void*)&recv , sizeof(unsigned) , TM_INFINITE) ;
sum += recv ;
done++;
}
rt_queue_delete(&queue ) ;
printf (,4.0f * ( ( float ) sum / ( float ) (THREADS * iterations ) ) , iterations ) ;
return 0 ;
}
