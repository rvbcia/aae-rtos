#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main() {

  int fd;
  char str[100];
  char * fifo = "/tmp/fifo";
  /* create the FIFO (named pipe) */
  mkfifo(fifo, 0666);
  
  /* write "Hi" to the FIFO */
  fd = open(fifo, O_WRONLY);
  gets(str);
  write(fd, str, sizeof(str));
  close(fd);
  
  /* remove the FIFO */
  unlink(fifo);
  return 0;
}
