#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>

volatile long int sum = 0;

pthread_mutex_t lock;

void adder(void *arg) {
    printf("Current sum = %ld\n", sum);
    long int number_to_add = (long int)arg;
    printf("Adding\n");
    sum += number_to_add;
    printf("Sum after adding = %ld\n\n", sum);
}

void long_adder(void *arg) {
    printf("Current sum = %ld\n", sum);
    long int number_to_add = (long int)arg;
    printf("Adding\n");
    for (int i = 0; i < 10000; ++i) {
        sum += number_to_add;
    }
    printf("Sum after adding = %ld\n\n", sum);
}


int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Pass a number to add as an argument!\n");
        exit(EXIT_FAILURE);
    }

    char *number_str = argv[1];
    long int number_to_add = atoi(number_str);
    
    pthread_t t[16];

    for (int i = 0; i < sizeof(t)/sizeof(pthread_t); ++i) {
        // create the threads and run adder() in each of them
        // pthread_create(&t[i], NULL, (void*)&adder, (void *)number_to_add);

        // as adder() may execute faster than pthread_create(), to show the shared variable problem
        // one may has to run adder_with_square() instead
        // pthread_create(&t[i], NULL, (void*)&long_adder, (void *)number_to_add);

        // Race condition handling with the use of MUTEX
        pthread_create(&t[i], NULL, (void*)&long_adder, (void *)number_to_add);
    }

    for (int i = 0; i < sizeof(t)/sizeof(pthread_t); ++i) {
        // wait for the threads to terminate
        pthread_join(t[i], NULL);
    }

    printf("Final sum = %ld\n", sum);
    return 0;
}