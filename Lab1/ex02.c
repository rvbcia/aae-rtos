#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>

volatile long int sum = 0;

pthread_mutex_t lock;

void long_adder_with_lock(void *arg) {
    printf("Current sum = %ld\n", sum);
    long int number_to_add = (long int)arg;
    long int buffer_number = 0;
    printf("Adding\n");
    for (int i = 0; i < 10000; ++i) {
        buffer_number += number_to_add;
    }
    pthread_mutex_lock(&lock);
    sum += buffer_number;
    pthread_mutex_unlock(&lock);
    printf("Sum after adding = %ld\n\n", sum);
}


int main(int argc, char *argv[]) {

    if (argc < 2) {
        printf("Pass a number to add as an argument!\n");
        exit(EXIT_FAILURE);
    }

    char *number_str = argv[1];
    long int number_to_add = atoi(number_str);
    
    pthread_t t[16];

    for (int i = 0; i < sizeof(t)/sizeof(pthread_t); ++i) {
        // Race condition handling with the use of MUTEX
        pthread_create(&t[i], NULL, (void*)&long_adder_with_lock, (void *)number_to_add);
    }

    for (int i = 0; i < sizeof(t)/sizeof(pthread_t); ++i) {
        pthread_join(t[i], NULL);
    }

    printf("Final sum = %ld\n", sum);
    return 0;
}