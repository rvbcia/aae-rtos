#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define BOOK_SIZE 20

volatile char book[BOOK_SIZE] = {0};

sem_t sem;

void write() {
    char message[4] = "noop";

    for (int j = 0; j < sizeof(message); ++j) {
        for (int i = 0; i < sizeof(book); ++i) {
            if (book[i] == 0) {
                book[i] = message[j];
                break;
            }
        }
    }
    
}

void read() {
    for (int i = 0; i < sizeof(book); ++i) {
        if (book[i] !=0) {
            printf("%c\n",book[i]);
        }
    }
}

int main(int argc, char *argv[]) {
    pthread_t writer, reader;
    pthread_create(&writer, NULL, (void *)write, NULL);
    pthread_create(&reader, NULL, (void *)read, NULL);
    pthread_join(writer, NULL);
    pthread_join(reader, NULL);
    return 0;
}