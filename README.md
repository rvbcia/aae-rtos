# AAE-RTOS

# Lab 1 - Threads in Linux

## Some theory

**Process** is an entity of a program run and managed by the Operating System. It has its own separate memory space. Each process has at least one thread.

**Thread** is an entity of some task run in a shared memory space of one process. Multiple threads (each responsible for a single task) may be run as a part of one process. Multiple threads are to be executed on multiple processor's cores.

**POSIX** (Portable Operating System Interface) is a standard enforced by IEEE implementing a set of automated conformance tests, which an OS has to pass in order to be POSIX compatible. They include C operations, multitasking, error states and command line behaviours and many more.

**MUTEX** (Mutual Exclusion) is a variable, which may "lock" the possibility to modify the variable. It can take values 0 or 1.

## Task 1 & 2

```ex01.c``` creates an array 16 threads, each of them running independently ```adder()```, adding a number passed as the CLI argument to the shared global variable ```sum```.

As ```adder()``` runtime is usually faster than ```pthread_create()```, to demonstrate the synchronization problem the ```long_adder()``` was written.

Multiple threads are accessing shared variable in the same time: one thread reads the variable value (e.g. 42) and performs calculations (adding value passed by the CLI multiplied by 10000), meanwhile another tread reads the same value (42) and performs the same calulations. The first thread stores the calculated value (e.g. 10042), after which the second thread does the same, resulting in loss of information.

To prevent this situaion a Mutex should be intruced, as was done in ```long_adder_with_lock()```.